int foo(int b);

int bazzy[4];
int bazzy0(void) { return bazzy[0] + 0; }
int bazzy1(void) { return bazzy[1] + 1; }
int bazzy2(void) { return bazzy[2] + 2; }
int bazzy3(void) { return bazzy[3] + 3; }

typedef int (*bazzy_t)(void);
bazzy_t funcs[4] = { bazzy0, bazzy1, bazzy2, bazzy3 };

int main(void)
{
   int a = foo(20);
   int b = foo(21);
   return funcs[0]() + funcs[3]() + a + b + b;
}
