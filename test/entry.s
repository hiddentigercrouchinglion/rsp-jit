.section .text
.set noat

.extern main
.globl __start
__start:
   li $sp, 0x00400ff8
   jal main

   move $k0, $v0
   break
   j __start
